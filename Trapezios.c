#include<stdio.h>
#include<stdlib.h>
#include<math.h>

// ***********************************
// Sergi Sales Jové: 191473
// Eric Monné Mesalles: 183338
// ***********************************
// Esse é o codigo em C da regra dos Trapezios para a aprocimação de integrais.
// Esse codigo pede o grado do polinomio, depois pega o polinomio, o intervalo e o numero de divisoes para a regra dos trapezios.
//

void readValues(float** polinomio, int polGrade, float *interval, int *accuracy);
void printValues(float * polinomio, float *interval, int *accuracy, int polGrade);
void calulateTriangles(float * polinomio, float *interval, int *accuracy, int polGrade);
float calculateArea(float step, float  x, float max,float *polinomio, int polGrade);
float calculatePolonimio(float value, float *polinomio, int polGrade);

int main() {

  int *accuracy;
  float *interval, *polinomio;
  int polGrade;
  accuracy = (int *)malloc(sizeof(int));
  interval = (float *)malloc(2 * sizeof(float));
  //colocamos um pequeno espaço para poder realocar depois, adaptando o tamanaho a cada loop
  polinomio = (float *)malloc(sizeof(float));
  char calculate = 's';

  while (calculate == 's') {
    printf("inserta o grado do polinomio: ");
    scanf("%d", &polGrade );
    readValues(&polinomio, polGrade, interval, accuracy);
    printValues(polinomio, interval, accuracy, polGrade);
    calulateTriangles(polinomio, interval, accuracy, polGrade);
    printf("Deseja calcular outro polinomio? [s/n] ");
    scanf(" %c",&calculate);
  }
}

// esa funçao é a que vai gestionar tudo o calculo do polinomio
void calulateTriangles(float * polinomio, float *interval, int *accuracy, int polGrade) {
  float step;
  float x;
  float area = 0;
  step = (interval[1] - interval[0]) / *accuracy;

  for(x = interval[0]; x < interval[1]; x+= step){
    float max = x + step;
    // iteramos do minimo ao maximo passando o calculo de cada step a uma subfunção
    area += calculateArea(step, x, max, polinomio, polGrade);
  }
  printf("\nresult:\n %0.4f \n\n",area);
}


// caculamos o polinomio para os valores determinados
float calculatePolonimio(float value, float *polinomio, int polGrade) {
  float result = 0;
  int x = 0;
  int i = 0;
  for (x = polGrade; x >= 0; x--) {
    float tempValue = 1;
    for(i = 0; i < x; i++ ) {
      tempValue *= value;
    }
    result = (polinomio[x] * tempValue) + result;
  }
  return result;
}

// calculamos la area do polinomio, left he o valor do catedo isquerdo é right o do dereito
float calculateArea(float step, float  x, float max,float *polinomio, int polGrade){
  float left = calculatePolonimio(x, polinomio, polGrade);
  float right = calculatePolonimio(max, polinomio, polGrade);
  return  step * ((left + right) / 2);
}


void printValues (float *polinomio, float *interval, int *accuracy, int polGrade) {
  int x;
  printf("\n\n------------- VALORES INSERIDOS -------------\n");
  printf("polinomio: \n ");
  for(x = polGrade; x >= 0; x--) {
    printf("+ %0.2f x^%d ", polinomio[x],x);
  }
  printf("\ngrado:\n %d \n", polGrade);

  printf("Intervalo:\n minimo: %0.2f maximo: %0.2f", interval[0],interval[1]);
  printf("\n");

  printf("numero de divisões:\n %d", *accuracy);
  printf("\n");

  printf("----------------------------------------------\n");
}

// funçao que vai ler os dados do usuario
void readValues(float** polinomio, int polGrade, float *interval, int *accuracy) {
  int x;
  *polinomio = (float *)realloc(*polinomio,(polGrade + 1) * sizeof(float));

  printf("inserta o polinomio: \n");
  // salvamos os valores do polinomio no array onde o indice é o valor do exponente da X
  for(x = polGrade; x >= 0; x--) {
    printf("x^%d: ", x );
    scanf("%f", *polinomio+x);
  }

  printf("inserta o valor minimo do intervalo: ");
  scanf("%f", interval);
  printf("inserta o valor maximo do intervalo: ");
  scanf("%f", interval+1);
  // evitamos que o intervalo seja errado, aseguranto que o minimo é menor que o maximo
  while(interval[0] > interval[1]){
    printf("--- ERROR: porfavor introducir rango valido ---\n");
    printf("inserta o valor minimo do intervalo: ");
    scanf("%f", interval);
    printf("inserta o valor maximo do intervalo: ");
    scanf("%f", interval+1);
  }
  printf("Insera o numero de divisões: ");
  scanf("%i", accuracy);

}
