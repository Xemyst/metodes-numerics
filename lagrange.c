#include<stdio.h>
#include<stdlib.h>

// *********************************
// Sergi Sales Jové: 191473
// Eric Monné Mesalles: 183338
// ***********************************

float calcLag(float **tabela, int rows);


int main () {

  float **tabela; // a matriz de duas dimensoes, dimensao 0 -> x; 1 -> f(x)
  int rows, x, i;
  float insertedX, px;
  printf("quantas filhas tem a sua tabela? "); //inserimos a cantidade da valors da nossa tabela
  scanf("%d",&rows);

  tabela = (float **)malloc(rows*sizeof(float)); //resevamos o espaço da tabela de dois dimensoes ao tamanho inserido por o usuario

  for(i=0; i<2; i++)
    tabela[i] = (float *)malloc(2*sizeof(float)); // reservamos os espaços para guardar ints na nosa matriz

  printf("inserte os valores: \n");
  for(x = 0; x < rows; x++){ //o usuario vai inserir os valos da tabela
    printf("x: ");
    scanf("%f",&tabela[0][x]);
    printf("f(x): ");
    scanf("%f",&tabela[1][x]);
  }

  printf("tabela inserida: \n"); // mostramos a tabela inserida
  for(x = 0; x < rows; x++) {
    printf("X: %0.3f f(x): %0.3f \n",tabela[0][x],tabela[1][x]);
  }

  px = calcLag(tabela, rows);

  free(tabela); //liberamos a memoria ocupada por a tabela
  return 0;
}


float calcLag(float **tabela, int rows) {
  int x, i;
  float px = 0, insertedX,num,den; // inicializamos os valores, den: vai ser o denominador da equacao, num o numerador  e px o valor resultante
  printf("inserte a X para achar o valor px(x): ");
  scanf("%f",&insertedX);
  for(x=0; x<rows; x++){ //vamos recorrer todos os valores da nossa tabela.
    num=1; // inicializamos os valos a 1 dado que se multiplamos por 0 vai estragar o resultado
    den=1;
    for(i=0; i<rows; i++){ // calculo de Li
      if(x!=i){ // si e o mesmo valor que o externo vamos a pular ele
        num=num*(insertedX-tabela[0][i]);
        den=den*(tabela[0][x]-tabela[0][i]);
      }
    }
    px=px+((num/den)*tabela[1][x]); // aplicamos onde o num/den é Li e multiplicamos por f(x) é sumamos ao valor acumulado
  }
  printf("el valor de px(%0.3f) é: %0.3f",insertedX, px); // printamos o resultado px

  return px;
}
