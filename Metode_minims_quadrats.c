#include<stdio.h>
#include<stdlib.h>

// ***********************************
// Sergi Sales Jové: 191473
// Eric Monné Mesalles: 183338
// ***********************************

//*******Declaraçao de funçoes********
void printarTabela(float **tabela, int rows);
void insertarTabela(float **tabela, int rows);
void ProdutoEscalarRecta(float **tabela, float **produtoEscalar,int rows);
void multiplicacao(float *dest, float *v1, float *v2, int size );
void eliminacaoGauss(float **temporal, int tamanho);
void resolverRecta(float **temporal, float *resultado);
void recta(float **tabela, float **temporal, int rows, float *resultado);
void produtoEscalarParabola(float **tabela, float **produtoEscalar ,int rows);
void parabola(float **tabela, float **temporal, int rows, float *resultado);
void resolverParabola(float **temporal, float *resultado);

int main () {

  float **tabela; // a matriz de duas dimensoes, dimensao 0 -> x; 1 -> f(x)
  float *resultado; // 0 -> X, 1 -> Y
  int rows, i;
  float **temporal; //onde operaremos a eliminação de gaus e produto escalar
  int type;


  printf("quantas filhas tem a sua tabela? "); //inserimos a cantidade da valors da nossa tabela
  scanf("%d",&rows);


  resultado = (float *)malloc(2*sizeof(float));


  tabela = (float **)malloc(2*sizeof(float)); //resevamos o espaço da tabela de dois dimensoes ao tamanho inserido por o usuario
  for(i=0; i<2; i++)
    tabela[i] = (float *) malloc (rows*sizeof(float)); // reservamos os espaços para guardar ints na nosa matriz

  insertarTabela(tabela, rows);
  printarTabela(tabela, rows);
  printf("Digite uma opçao (1) reta (2) parabola: ");
  scanf("%d", &type );

  if ( type == 1 ) {
    temporal = (float **)malloc(2*sizeof(float));
    for(i=0; i<2; i++)
      temporal[i] = (float *)malloc(3*sizeof(float));
    resultado = (float *)malloc(2*sizeof(float));
    recta( tabela, temporal, rows, resultado);
  } else if ( type == 2 ){
    temporal = (float **)malloc(3*sizeof(float));
    for(i=0; i<3; i++)
      temporal[i] = (float *)malloc(4*sizeof(float));
    resultado = (float *)malloc(3*sizeof(float));
    parabola(tabela, temporal, rows, resultado);
  }

  free(tabela); //liberamos a memoria ocupada por a tabela
  free(resultado);
  free(temporal);

  return 0;
}

//********* Funçoes da Parabola **********
void parabola(float **tabela, float **temporal, int rows, float *resultado) {
  produtoEscalarParabola(tabela, temporal, rows);
  eliminacaoGauss(temporal, 3);
  resolverParabola(temporal, resultado);
}

void resolverParabola(float **temporal, float *resultado) {

  resultado[0] = temporal[2][3] / temporal [2][2];
  resultado[1] = (temporal [1][3] - resultado[0] * temporal[1][2] ) / temporal[1][1];
  resultado[2] = (temporal [0][3] - (temporal[0][2] * resultado[0] + temporal[0][1]  * resultado[1])) / temporal[0][0];

  printf("y: %0.3fx^2 + %0.3fx + %0.3f \n", resultado[0],resultado[1], resultado[2]);
}



void produtoEscalarParabola(float **tabela, float **produtoEscalar ,int rows) {
  int x = 0;
  float *u;
  u = (float *)malloc(rows*sizeof(float));
  for(x = 0; x < rows; x++){
    u[x] = 1;
  }
  float *u2;
  u2 = (float *)malloc(rows*sizeof(float));

  for(x = 0; x < rows; x++)
    u2[x] = tabela[0][x] * tabela[0][x];

  multiplicacao(&produtoEscalar[0][0], u, u, rows);
  multiplicacao(&produtoEscalar[0][1], u, tabela[0], rows);
  multiplicacao(&produtoEscalar[0][2], u, u2, rows);
  multiplicacao(&produtoEscalar[0][3], u, tabela[1], rows);

  multiplicacao(&produtoEscalar[1][0], tabela[0], u, rows);
  multiplicacao(&produtoEscalar[1][1], tabela[0], tabela[0], rows);
  multiplicacao(&produtoEscalar[1][2], tabela[0], u2, rows);
  multiplicacao(&produtoEscalar[1][3], tabela[0], tabela[1], rows);

  multiplicacao(&produtoEscalar[2][0], u2, u, rows);
  multiplicacao(&produtoEscalar[2][1], u2, tabela[0], rows);
  multiplicacao(&produtoEscalar[2][2], u2, u2, rows);
  multiplicacao(&produtoEscalar[2][3], u2, tabela[1], rows);

  printf("\n---------------- produto escalar:  --------------- \n");
  for(x = 0; x < 3; x ++)
    printf("%0.3f  %0.3f  %0.3f | %0.3f \n", produtoEscalar[x][0],produtoEscalar[x][1],produtoEscalar[x][2],produtoEscalar[x][3]);
  printf("------------------------------------------------- \n");


  free(u);
}

//************ Funçoes da Reta *************

void recta(float **tabela, float **temporal, int rows, float *resultado) {
  ProdutoEscalarRecta(tabela, temporal, rows);
  eliminacaoGauss(temporal, 2);
  resolverRecta(temporal, resultado);
}

void resolverRecta(float **temporal, float *resultado) {

  resultado[0] = temporal[1][2] / temporal [1][1];
  resultado[1] = (temporal [0][2] - resultado[0] * temporal[0][1] ) / temporal[0][0];

  printf("y: %0.3fx + %0.3f \n", resultado[0],resultado[1]);
}

void ProdutoEscalarRecta(float **tabela, float **produtoEscalar ,int rows) {
  int x = 0;
  float *u;
  u = (float *)malloc(rows*sizeof(float));
  for(x = 0; x < rows; x++){
    u[x] = 1;
  }

  multiplicacao(&produtoEscalar[0][0], u, u, rows);
  multiplicacao(&produtoEscalar[0][1], u, tabela[0], rows);
  multiplicacao(&produtoEscalar[0][2], u, tabela[1], rows);
  multiplicacao(&produtoEscalar[1][0], u, tabela[0], rows);
  multiplicacao(&produtoEscalar[1][1], tabela[0], tabela[0], rows);
  multiplicacao(&produtoEscalar[1][2], tabela[0], tabela[1], rows);

  printf("\n---------------- produto ecalar:  --------------- \n");
  for(x = 0; x < 2; x ++)
    printf("%0.3f  %0.3f | %0.3f \n", produtoEscalar[x][0],produtoEscalar[x][1],produtoEscalar[x][2]);
  printf("------------------------------------------------- \n");


  free(u);
}

//******* Funçoes da eliminaçao de Gaus ********

void eliminacaoGauss(float **temporal, int tamanho)  {
  int step = 0;
  int x = 0, y = 0;
  float pivot;
  for ( step = 0; step < tamanho - 1; step++ ) {
    for (x = step + 1; x < tamanho; x ++) {
      pivot = temporal[x][step] / temporal[step][step];
      for (y = step; y <= tamanho; y++) {
        temporal[x][y] -= (temporal[step][y] * pivot);
      }
    }
  }

  printf("-------- eliminacao Gaus ---------- \n");
  for(step = 0; step < tamanho; step ++){
    for(x = 0; x < tamanho; x++){
      printf("%0.3f ", temporal[step][x] );
    }
    printf(" | %0.3f\n", temporal[step][x] );
  }
  printf("----------------------------------- \n");

}

//******Multiplicador de matrizes**********

void multiplicacao(float *dest, float *v1, float *v2, int size) {
  int x = 0;
  dest[0] = 0;
  for( x = 0; x<size; x++) {
    dest[0] += (v1[x] * v2[x]);
  }

}
void insertarTabela (float **tabela, int rows) {
  int x;
  printf("inserte os valores: \n");
  for(x = 0; x < rows; x++){ //o usuario vai inserir os valos da tabela
    printf("X[%d]: ",x);
    scanf("%f",&tabela[0][x]);
  }
  for(x = 0; x < rows; x++){ //o usuario vai inserir os valos da tabela
    printf("Y[%d]: ",x);
    scanf("%f",&tabela[1][x]);
  }
}
void printarTabela (float **tabela, int rows) {

  int x;
  printf("tabela inserida: \n"); // mostramos a tabela inserida
  printf("  X   |  Y   \n");
  printf("-------------\n");
  for(x = 0; x < rows; x++) {
    printf("%0.3f|%0.3f \n",tabela[0][x],tabela[1][x]);
  }
  printf("-------------\n");
}
